import numpy as np

class NeuralNetwork(object):
    def __init__(self, input_nodes, hidden_nodes, output_nodes, learning_rate):
        # Set number of nodes in input, hidden and output layers.
        self.input_nodes = input_nodes
        self.hidden_nodes = hidden_nodes
        self.output_nodes = output_nodes

        # Initialize weights
        self.weights_input_to_hidden = np.random.normal(0.0, self.hidden_nodes**-0.5, (self.hidden_nodes, self.input_nodes))

        self.weights_hidden_to_output = np.random.normal(0.0,  self.output_nodes**-0.5, (self.output_nodes, self.hidden_nodes))
        self.lr = learning_rate
        
        #### TODO: Set self.activation_function to your implemented sigmoid function ####
        #
        # Note: in Python, you can define a function with a lambda expression,
        # as shown below.
        self.activation_function = lambda x : (1 / (1 + np.exp(-x)))  # Replace 0 with your sigmoid calculation.
        
        self.activation_function_prime = lambda x : (self.activation_function(x) * (1 - self.activation_function(x)))
        
        ### If the lambda code above is not something you're familiar with,
        # You can uncomment out the following three lines and put your 
        # implementation there instead.
        #
        #def sigmoid(x):
        #    return 0  # Replace 0 with your sigmoid calculation here
        #self.activation_function = sigmoid
                    
    
    def train(self, inputs_list, targets_list):
        # Convert inputs list to 2d array
        inputs = np.array(inputs_list, ndmin=2)
        targets = np.array(targets_list, ndmin=2)
        print(inputs)
        
        delta_weights_i_h = np.zeros(self.weights_input_to_hidden.shape)
        delta_weights_h_o = np.zeros(self.weights_hidden_to_output.shape)

        for raw_x, y in zip(inputs, targets):
            X = np.array(raw_x, ndmin = 2).T
            hidden_inputs = self.weights_input_to_hidden.dot(X)
            hidden_outputs = self.activation_function(hidden_inputs)

            final_inputs = self.weights_hidden_to_output.dot(hidden_outputs)
            final_outputs = self.activation_function(final_inputs)

            error = y - final_outputs
            output_error_term = error * final_outputs * (1 - final_outputs)

            hidden_error = output_error_term.dot(self.weights_hidden_to_output)

            # hidden_output_error_term = output_error_term.dot(self.weights_hidden_to_output)
            hidden_input_error_term = hidden_error.dot(hidden_outputs * (1 - hidden_outputs))

            delta_weights_i_h += X.dot(hidden_input_error_term)
            delta_weights_h_o += output_error_term.dot(hidden_outputs.T)

        self.weights_hidden_to_output += self.lr * delta_weights_h_o / inputs.shape[0]
        self.weights_input_to_hidden += self.lr * delta_weights_i_h / inputs.shape[0]

        
    def run(self, inputs_list):
        # Run a forward pass through the network
        inputs = np.array(inputs_list, ndmin=2).T
        
        #### Implement the forward pass here ####
        # TODO: Hidden layer - replace these values with the appropriate calculations.
        hidden_inputs = self.weights_input_to_hidden.dot(inputs) # signals into hidden layer
        hidden_outputs = self.activation_function(hidden_inputs) # signals from hidden layer
        
        # TODO: Output layer - Replace these values with the appropriate calculations.
        final_inputs = self.weights_hidden_to_output.dot(hidden_outputs) # signals into final output layer
        final_outputs = self.activation_function(final_inputs) # signals from final output layer 
        
        return final_outputs