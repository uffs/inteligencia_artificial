Neste projeto, você vai fazer uma rede neural para resolver um problema de predição em um conjunto de dados real! Construindo uma rede neural do zero, você vai entender muito melhor como funcionam o gradiente descendente, backpropagation e outros conceitos importantes antes de começarmos a usar ferramentas como Tensorflow. Você também terá uma chance de ver sua rede resolvendo um problema real!

Os dados vêm da UCI Machine Learning Database.

Instruções
Faça o download do material do projeto do nosso repositório github. Você pode obter o repositório completo com o comando git clone https://github.com/udacity/deep-learning.git, ou baixando um zip pelo site.
Entre na pasta first-neural-network pelo terminal.
Faça o download do anaconda ou miniconda, seguindo as instruções na lição sobre Anacoda, contida na introdução do curso.
Crie um novo ambiente conda
conda create --name dlnd python=3
Entre no seu ambiente:
Mac/Linux: >> source activate dlnd
Windows: >> activate dlnd
Certifique-se de que o numpy, matplotlib, pandas e jupyter notebook estejam instalados:
conda install numpy matplotlib pandas jupyter notebook
Abra o notebook:
jupyter notebook
No navegador, abra o arquivo Your_first_neural_network.ipynb
Siga as instruções no notebook que te conduzem através do projeto.
Certifique-se de que você os testes do notebook passaram antes de submeter o projeto!