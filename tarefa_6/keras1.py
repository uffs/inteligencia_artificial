import numpy as np
from keras.utils import np_utils
import tensorflow as tf
# Using TensorFlow 1.0.0; use tf.python_io in later versions
#tf.python.control_flow_ops = tf.python_io

# Set random seed
np.random.seed(42)

# Our data
X = np.array([
    [0,0],
    [0,1],
    [1,0],
    [1,1]
]).astype('float32')
y = np.array([
    [0],
    [1],
    [1],
    [0]]).astype('float32')

# Initial Setup for Keras
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.optimizers import SGD

# Building the model
xor = Sequential()

# Add required layers
# xor.add()

# Specify loss as "binary_crossentropy", optimizer as "adam",
# and add the accuracy metric
# xor.compile()

xor.add(Dense(8, input_dim = X.shape[1]))
xor.add(Activation('tanh'))
xor.add(Dense(1))
xor.add(Activation('sigmoid'))

xor.compile(
    'Adagrad',
    loss = 'binary_crossentropy',
    metrics = ['accuracy']
)

# Uncomment this line to print the model architecture
xor.summary()

# Fitting the model
history = xor.fit(X, y, epochs = 50, verbose = 0)

# Scoring the model
score = xor.evaluate(X, y)
print("\nAccuracy: ", score)

# Checking the predictions
print("\nPredictions:")
print(xor.predict_proba(X))
